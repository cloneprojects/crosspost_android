package com.app.crosspost.agoraGroupCall.openvcall.ui;

import android.view.View;

public interface VideoViewEventListener {
    void onItemDoubleClick(View v, Object item);
}
