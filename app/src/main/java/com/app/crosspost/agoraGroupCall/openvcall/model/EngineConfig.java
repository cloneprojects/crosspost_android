package com.app.crosspost.agoraGroupCall.openvcall.model;

public class EngineConfig {
    public int mVideoProfile;

    public int mUid;

    public String mChannel;

    public void reset() {
        mChannel = null;
    }

    EngineConfig() {
    }
}
