package com.app.crosspost.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vanniktech.emoji.EmojiTextView;
import com.app.crosspost.DBHelper.DBHandler;
import com.app.crosspost.R;
import com.app.crosspost.Service.ServiceClasss;
import com.app.crosspost.baseUtils.CustomDialog;
import com.app.crosspost.baseUtils.SharedHelper;
import com.app.crosspost.baseUtils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class ScheduleText extends AppCompatActivity {

    private static final String TAG = ScheduleText.class.getName();
    public RecyclerView scheduledMessages;
    private ScheduledMessagesRecycler scheduleMessage;
    public static List<JSONObject> scheduledMsgs;
    Toolbar schedule_toolbar;
    private List<JSONObject> scheduledMsgsList;
    int iSize = 1;
    String themevalue;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void Setheme(String themevalue) {
        try {
            switch (themevalue) {
                case "1":
                    setTheme(R.style.AppThemeGreen);
                    setGradientColor(1);
                    break;
                case "2":
                    setTheme(R.style.AppThemeBlue);
                    setGradientColor(2);
                    break;
                case "3":
                    setTheme(R.style.AppThemeIndigo);
                    setGradientColor(3);
                    break;
                case "4":
                    setTheme(R.style.AppThemeGrey);
                    setGradientColor(4);
                    break;
                case "5":
                    setTheme(R.style.AppThemeYellow);
                    setGradientColor(5);
                    break;
                case "6":
                    setTheme(R.style.AppThemeOrange);
                    setGradientColor(6);
                    break;
                case "7":
                    setTheme(R.style.AppThemePurple);
                    setGradientColor(7);
                    break;
                case "8":
                    setTheme(R.style.AppThemePaleGreen);
                    setGradientColor(8);
                    break;
                case "9":
                    setTheme(R.style.AppThemelightBlue);
                    setGradientColor(9);
                    break;
                case "10":
                    setTheme(R.style.AppThemePink);
                    setGradientColor(10);
                    break;
                case "11":
                    setTheme(R.style.AppThemelightGreen);
                    setGradientColor(11);
                    break;
                case "12":
                    setTheme(R.style.AppThemelightRed);
                    setGradientColor(12);
                    break;
                default:
                    setTheme(R.style.AppThemeGreen);
                    setGradientColor(1);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setGradientColor(int i) {

        try {
            schedule_toolbar.setBackgroundColor(getPrimaryCOlor(ScheduleText.this));
//            if (i == 1) {
//                schedule_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_colour));
//            } else if (i == 2) {
//                schedule_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_teo));
//            } else if (i == 3) {
//                schedule_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_three));
//            } else if (i == 4) {
//                schedule_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_four));
//            } else if (i == 5) {
//                schedule_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_five));
//            } else if (i == 6) {
//                schedule_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_six));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getPrimaryCOlor(Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }

    public void setIconColour(Drawable drawable, int primaryCOlor) {

        drawable.setColorFilter(new PorterDuffColorFilter(getPrimaryCOlor(ScheduleText.this), PorterDuff.Mode.SRC_IN));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        themevalue = SharedHelper.getKey(this, "theme_value");
        Setheme(themevalue);
        setContentView(R.layout.activity_schedule_text);

        ImageView next_icon = (ImageView) findViewById(R.id.next_icon);
        setIconColour(next_icon.getDrawable(), getPrimaryCOlor(ScheduleText.this));
        schedule_toolbar = (Toolbar) findViewById(R.id.schedule_toolbar);
        scheduledMessages = (RecyclerView) findViewById(R.id.scheduledMessages);
        Setheme(themevalue);
        findViewById(R.id.backPressed).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

        findViewById(R.id.textschedule).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ScheduleText.this, ScheduleChatActivity.class));
            }
        });


    }


    @Override
    protected void onResume() {
        try {
            DBHandler dbHandler = new DBHandler(ScheduleText.this);
            scheduledMsgsList = dbHandler.GetScheduledMessages();
            if (scheduledMsgsList.size() > 0) {

//
//                for (int i = 0; i < scheduledMsgsList.size(); i++) {
//                    iSize++;
//                    final JSONArray jsonObject = new JSONArray(scheduledMsgsList);

//                    if (System.currentTimeMillis()>=Long.parseLong(jsonObject.optJSONObject(i).optString("deliverTime"))) {
//
//
//                        dbHandler.scheduleTextUpdate("Sent", jsonObject.optJSONObject(i).optString("groupId"));
//                    }


//                    scheduledMsgsList = dbHandler.GetScheduledMessages();
                Collections.reverse(scheduledMsgsList);
//                    if (i == scheduledMsgsList.size() - 1) {
                try {

//                            if (scheduledMsgsList.size() > 0) {

                    scheduleMessage = new ScheduledMessagesRecycler(ScheduleText.this, scheduledMsgsList);
                    LinearLayoutManager verticalLayoutmanager
                            = new LinearLayoutManager(ScheduleText.this, LinearLayoutManager.VERTICAL, false);
                    ;
                    scheduledMessages.setLayoutManager(verticalLayoutmanager);
                    scheduledMessages.setFocusable(false);
                    scheduledMessages.setAdapter(scheduleMessage);
//                            }
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                    }
//                }


            }

            Log.e(TAG, "onResume: " + scheduledMsgsList);
        } catch (Exception e) {
            e.printStackTrace();
        }


//
        super.onResume();
    }


    private class ScheduledMessagesRecycler extends RecyclerView.Adapter<ScheduledMessagesRecycler.MyViewHolder> {
        List<JSONObject> scheduleMsg;
        Context context;
        String date, times;

        public ScheduledMessagesRecycler(Context newGroup_activity, List<JSONObject> list_of_schedule_msg) {
            scheduleMsg = new ArrayList<>();
            scheduleMsg = list_of_schedule_msg;
            this.context = newGroup_activity;

        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.scheduled_list, parent, false);
            return new ScheduledMessagesRecycler.MyViewHolder(view);
        }


        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            final JSONArray jsonObject = new JSONArray(scheduleMsg);

            try {
                holder.user_status.setText(jsonObject.optJSONObject(position).optString("message"));


                if (jsonObject.optJSONObject(position).optString("status").equalsIgnoreCase("Pending")) {
                    holder.messages_status.setTextColor(Color.parseColor("#00897b"));
                    holder.itemView.setClickable(true);
                    holder.itemView.setEnabled(true);
                    holder.itemView.setLongClickable(false);
                    holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {


                            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogCustom);

                            builder.setTitle("Alert");

                            builder.setMessage("Are you sure you want to cancel this schedule message?");

                            builder.setPositiveButton(Html.fromHtml("<font color='#00897b'>OK</font>"), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (!Utils.isNetworkAvailable(context)) {
                                        Utils.showShortToast(getResources().getString(R.string.no_internet), context);
                                    } else {
                                        deletechat(jsonObject.optJSONObject(position).optString("groupId"), jsonObject.optJSONObject(position).optString("userId"), jsonObject.optJSONObject(position).optString("deliverTime"));
                                    }
                                    dialog.dismiss();
                                }
                            });
                            builder.setNegativeButton(Html.fromHtml("<font color='#00897b'>Cancel</font>"), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builder.show();


                            return true;
                        }
                    });

                } else {
                    holder.messages_status.setTextColor(Color.parseColor("#ff0000"));
                    holder.itemView.setClickable(true);
                    //holder.itemView.setEnabled(false);

                    holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View view) {


                            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogCustom);

                            builder.setTitle("");

                            builder.setMessage("Are you sure you want to delete this schedule message?");

                            builder.setPositiveButton(Html.fromHtml("<font color='#00897b'>OK</font>"), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (!Utils.isNetworkAvailable(context)) {
                                        Utils.showShortToast(getResources().getString(R.string.no_internet), context);
                                    } else {
                                        deletechatfromDb(jsonObject.optJSONObject(position).optString("groupId"), jsonObject.optJSONObject(position).optString("userId"), jsonObject.optJSONObject(position).optString("deliverTime"));
                                    }
                                    dialog.dismiss();
                                }
                            });
                            builder.setNegativeButton(Html.fromHtml("<font color='#00897b'>Cancel</font>"), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builder.show();


                            return true;
                        }
                    });
                }


                holder.messages_status.setText(jsonObject.optJSONObject(position).optString("status"));


                long deliverDate = jsonObject.optJSONObject(position).optLong("deliverTime");
                long deliverTime = jsonObject.optJSONObject(position).optLong("deliverTime");

                date = Utils.getDate((long) deliverDate, "dd-MM-yyyy");
                times = Utils.getDate((long) deliverTime, "hh:mm a");

                holder.msg_date.setText(String.valueOf(date));
                holder.username.setText(jsonObject.optJSONObject(position).optString("userName"));
                holder.lastMsgTime_text.setText(String.valueOf(times));

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        CustomDialog photoViewer = new CustomDialog(context);
                        photoViewer.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        themevalue = SharedHelper.getKey(context, "theme_value");

                        long deliverDate = jsonObject.optJSONObject(position).optLong("deliverTime");
                        long deliverTime = jsonObject.optJSONObject(position).optLong("deliverTime");

                        date = Utils.getDate((long) deliverDate, "dd-MM-yyyy");
                        times = Utils.getDate((long) deliverTime, "hh:mm a");

                        photoViewer.setContentView(R.layout.popup_schedule);
                        photoViewer.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                        photoViewer.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        photoViewer.getWindow().setGravity(Gravity.CENTER);
                        photoViewer.setCanceledOnTouchOutside(true);
                        photoViewer.show();
                        photoViewer.getWindow().getAttributes().windowAnimations = R.anim.media_in_animation;
                        // myBottomSheet.show(getSupportFragmentManager(), myBottomSheet.getTag());

                        TextView text_name_per = (TextView) photoViewer.findViewById(R.id.text_name_per);
                        text_name_per.setText(jsonObject.optJSONObject(position).optString("userName"));

                        TextView txt_title = (TextView) photoViewer.findViewById(R.id.txt_title);
                        txt_title.setText(jsonObject.optJSONObject(position).optString("userName"));

                        TextView txt_scheduled_date = (TextView) photoViewer.findViewById(R.id.txt_scheduled_date);
                        txt_scheduled_date.setText("scheduled @ " + date + " " + times);

                        TextView sender_message_text = (TextView) photoViewer.findViewById(R.id.sender_message_text);
                        sender_message_text.setText(jsonObject.optJSONObject(position).optString("message"));
                        Toolbar popup_toolbar = (Toolbar) photoViewer.findViewById(R.id.popup_toolbar);
                        setToolbarBackGroound(popup_toolbar, Integer.parseInt(themevalue));

//                        try {
//                            if (Integer.parseInt(themevalue) == 1) {
//                                popup_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_colour));
//                            } else if (Integer.parseInt(themevalue) == 2) {
//                                popup_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_teo));
//                            } else if (Integer.parseInt(themevalue) == 3) {
//                                popup_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_three));
//                            } else if (Integer.parseInt(themevalue) == 4) {
//                                popup_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_four));
//                            } else if (Integer.parseInt(themevalue) == 5) {
//                                popup_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_five));
//                            } else if (Integer.parseInt(themevalue) == 6) {
//                                popup_toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_six));
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }

                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return scheduleMsg.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView msg_date, lastMsgTime_text, messages_status, text_name_per, username;
            EmojiTextView user_status;
            RelativeLayout textmessage;
            ImageView icon;

            public MyViewHolder(View itemView) {
                super(itemView);

                msg_date = (TextView) itemView.findViewById(R.id.msg_date);
                lastMsgTime_text = (TextView) itemView.findViewById(R.id.lastMsgTime_text);
                messages_status = (TextView) itemView.findViewById(R.id.messages_status);
                user_status = (EmojiTextView) itemView.findViewById(R.id.user_status);
                textmessage = (RelativeLayout) itemView.findViewById(R.id.textmessage);
                text_name_per = (TextView) itemView.findViewById(R.id.text_name_per);
                username = (TextView) itemView.findViewById(R.id.username);
                icon = (ImageView) itemView.findViewById(R.id.icon_new);
                setIconColour(icon.getDrawable(), getPrimaryCOlor(context));

            }
        }

        public int getPrimaryCOlor(Context context) {
            final TypedValue value = new TypedValue();
            context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
            return value.data;
        }

        public void setIconColour(Drawable drawable, int primaryCOlor) {

            drawable.setColorFilter(new PorterDuffColorFilter(getPrimaryCOlor(context), PorterDuff.Mode.SRC_IN));

        }

    }


    @SuppressLint("StaticFieldLeak")
    private void deletechat(final String groupId, final String userId, final String time) {
        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Utils.showSimpleProgressDialog(ScheduleText.this, "Please wait...", false);


            }


            @Override
            protected String doInBackground(String... strings) {


                deleteScheduleFromServer(groupId, userId, time);

                DBHandler dbHandler = new DBHandler(ScheduleText.this);
                dbHandler.deleteScheduleText(groupId, userId);
                scheduledMsgsList = dbHandler.GetScheduledMessages();
                Collections.reverse(scheduledMsgsList);
                dbHandler.close();


                //Utils.showSimpleProgressDialog(ScheduleText.this, "Work done...", false);
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
//                Utils.removeProgressDialog();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

//                            if (scheduledMsgsList.size() > 0) {
                            scheduleMessage = new ScheduledMessagesRecycler(ScheduleText.this, scheduledMsgsList);
                            scheduledMessages.setAdapter(scheduleMessage);
//                            }
                        } catch (Exception e) {

                        }
                    }
                });


            }
        }.execute();

    }


    @SuppressLint("StaticFieldLeak")
    private void deletechatfromDb(final String groupId, final String userId, final String time) {
        new AsyncTask<String, String, String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Utils.showSimpleProgressDialog(ScheduleText.this, "Please wait...", false);


            }


            @Override
            protected String doInBackground(String... strings) {

                DBHandler dbHandler = new DBHandler(ScheduleText.this);
                dbHandler.deleteScheduleText(groupId, userId);
                scheduledMsgsList = dbHandler.GetScheduledMessages();
                Collections.reverse(scheduledMsgsList);
                dbHandler.close();


                //Utils.showSimpleProgressDialog(ScheduleText.this, "Work done...", false);
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                Utils.removeProgressDialog();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

//                           if (scheduledMsgsList.size() > 0) {
                            scheduleMessage = new ScheduledMessagesRecycler(ScheduleText.this, scheduledMsgsList);
                            scheduledMessages.setAdapter(scheduleMessage);
//                           }
                        } catch (Exception e) {

                        }
                    }
                });


            }
        }.execute();

    }


    private void deleteScheduleFromServer(String groupId, String userId, String time) {

        ServiceClasss.Emitters emitters = new ServiceClasss.Emitters(ScheduleText.this);
        AsyncTask<Void, Void, Void> obj = null;
        boolean messageStatus = emitters.deleteScheduleTextFromServer(groupId, userId, time, ScheduleText.this);


        Log.e(TAG, "messageStatus: " + messageStatus);

    }


    private void setToolbarBackGroound(Toolbar view, int i) {


        try {
            view.setBackgroundColor(getPrimaryCOlor(ScheduleText.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
