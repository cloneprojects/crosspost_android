package com.app.crosspost.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;
import com.app.crosspost.DBHelper.DBHandler;
import com.app.crosspost.R;
import com.app.crosspost.baseUtils.SharedHelper;
import com.app.crosspost.baseUtils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class MessageInfoDetailsActivity extends AppCompatActivity {

    RecyclerView seenRecyclerView, deliveredRecyclerView;
    SeenAdapter seenAdapter;
    DeliveredAdapter deliveredAdapter;
    JSONArray jsonArray;
    Toolbar toolbar;
    LinearLayout groupLayout, singleChatinfo;
    CollapsingToolbarLayout collapsingToolbarLayout;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void Setheme(String themevalue) {
        switch (themevalue) {
            case "1":
                setTheme(R.style.AppThemeGreen);
              //  setGradientColor(1);
                break;
            case "2":
                setTheme(R.style.AppThemeBlue);
              //  setGradientColor(2);
                break;
            case "3":
                setTheme(R.style.AppThemeIndigo);
              //  setGradientColor(3);
                break;
            case "4":
                setTheme(R.style.AppThemeGrey);
                //setGradientColor(4);
                break;
            case "5":
                setTheme(R.style.AppThemeYellow);
                //setGradientColor(5);
                break;
            case "6":
                setTheme(R.style.AppThemeOrange);
               // setGradientColor(6);
                break;
            case "7":
                setTheme(R.style.AppThemePurple);
                break;
            case "8":
                setTheme(R.style.AppThemePaleGreen);
                break;
            case "9":
                setTheme(R.style.AppThemelightBlue);
                break;
            case "10":
                setTheme(R.style.AppThemePink);
                break;
            case "11":
                setTheme(R.style.AppThemelightGreen);
                break;
            case "12":
                setTheme(R.style.AppThemelightRed);
                break;
            default:
                setTheme(R.style.AppThemeGreen);
                break;
        }
    }

    private void setGradientColor(int i) {
        try {
//            if (i == 1) {
//                toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_colour));
//            } else if (i == 2) {
//                toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_teo));
//            } else if (i == 3) {
//                toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_three));
//            } else if (i == 4) {
//                toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_four));
//            } else if (i == 5) {
//                toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_five));
//            } else if (i == 6) {
//                toolbar.setBackground(getResources().getDrawable(R.drawable.gradient_color_six));
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String themevalue = SharedHelper.getKey(this, "theme_value");
        Setheme(themevalue);
        setContentView(R.layout.activity_message_info_details);
        TextView read_time_text, delievered_time_text;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        Setheme(themevalue);
        read_time_text = (TextView) findViewById(R.id.read_time_text);
        delievered_time_text = (TextView) findViewById(R.id.delievered_time_text);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        groupLayout = (LinearLayout) findViewById(R.id.groupLayout);
        singleChatinfo = (LinearLayout) findViewById(R.id.singleChatinfo);
        deliveredRecyclerView = (RecyclerView) findViewById(R.id.deliveredRecyclerView);
        seenRecyclerView = (RecyclerView) findViewById(R.id.seenRecyclerView);


        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle("Message Info");
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        setSupportActionBar(toolbar);


        try {

            Intent intent = getIntent();
            if (intent != null) {

                String roomType = intent.getStringExtra("chatRoomType");




                if (roomType.equalsIgnoreCase("0")) {
                    singleChatinfo.setVisibility(View.VISIBLE);
                    groupLayout.setVisibility(View.GONE);
                    String singleChatArray = intent.getStringExtra("singleChatArray");
                    JSONArray jsonArray = new JSONArray(singleChatArray);
                    String readTime = null;
                    String deliveredTime = null;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (jsonArray.optJSONObject(i).optString("deliveredTime").length() > 0) {
                            deliveredTime = jsonArray.optJSONObject(i).optString("deliveredTime");
                            double time = Double.valueOf(deliveredTime);
                            delievered_time_text.setText(Utils.getDate((long) time, "dd-MM-yyyy hh:mm a"));
                        }
                        if (jsonArray.optJSONObject(i).optString("readTime").length() > 0) {
                            readTime = jsonArray.optJSONObject(i).optString("readTime");
                            double time = Double.valueOf(readTime);
                            read_time_text.setText(Utils.getDate((long) time, "dd-MM-yyyy hh:mm a"));
                        }

                    }

                } else {
                    singleChatinfo.setVisibility(View.GONE);
                    groupLayout.setVisibility(View.VISIBLE);
                    String listDelivered = intent.getStringExtra("messageArray");
                    String listSeen = intent.getStringExtra("seenArray");
                    JSONArray jsonArray = new JSONArray(listDelivered);
                    JSONArray jsonArraySeen = new JSONArray(listSeen);
                    if (jsonArray.length() > 0) {


                        deliveredAdapter = new DeliveredAdapter(MessageInfoDetailsActivity.this, jsonArray);
                        deliveredRecyclerView.setLayoutManager(new LinearLayoutManager(MessageInfoDetailsActivity.this));
                        deliveredRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(MessageInfoDetailsActivity.this)
                                .color(Color.parseColor("#dfdfdf"))
                                .sizeResId(R.dimen.divider)
                                .marginResId(R.dimen.leftmargin, R.dimen.rightmargin)
                                .build());
                        deliveredRecyclerView.setAdapter(deliveredAdapter);
                    }
                    if (jsonArraySeen.length() > 0) {


                        seenAdapter = new SeenAdapter(MessageInfoDetailsActivity.this, jsonArraySeen);
                        seenRecyclerView.setLayoutManager(new LinearLayoutManager(MessageInfoDetailsActivity.this));
                        seenRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(MessageInfoDetailsActivity.this)
                                .color(Color.parseColor("#dfdfdf"))
                                .sizeResId(R.dimen.divider)
                                .marginResId(R.dimen.leftmargin, R.dimen.rightmargin)
                                .build());
                        seenRecyclerView.setAdapter(seenAdapter);

                    }
                }


            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }



    private class SeenAdapter extends RecyclerView.Adapter<SeenAdapter.MyViewHolder> {
        private JSONArray array;
        private Context mContext;
        private String themevalue;

        public SeenAdapter(Activity activity, JSONArray jsonObject) {
            this.array = jsonObject;
            this.mContext = activity;
        }

        @Override
        public SeenAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.message_info_item_seen, parent, false);
            return new SeenAdapter.MyViewHolder(view);
        }


        @Override
        public void onBindViewHolder(final SeenAdapter.MyViewHolder holder, int position) {

            try {

                if (array.optJSONObject(position).optString("status").equalsIgnoreCase("Read")) {
                    DBHandler dbHandler = new DBHandler(mContext);
                    JSONObject jsonObject = dbHandler.GetUserInfo(array.optJSONObject(position).optString("toNum"));


                    holder.seenName.setText(jsonObject.optString("Name"));

                    double time = Double.valueOf(array.optJSONObject(position).optString("readTime"));
                    holder.seen_time_text.setText(Utils.getDate((long) time, "dd-MM-yyyy hh:mm a"));


                    Picasso.with(mContext).load(jsonObject.optString("Image")).placeholder(R.drawable.ic_account_circle).error(R.drawable.ic_account_circle).into(holder.profileSeen, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {

                        }
                    });

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return array.length();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            CircleImageView profileSeen;
            TextView seenName, seen_time_text, text_name_per;

            public MyViewHolder(View itemView) {
                super(itemView);
                profileSeen = (CircleImageView) itemView.findViewById(R.id.profileSeen);
                seenName = (TextView) itemView.findViewById(R.id.seenName);
                seen_time_text = (TextView) itemView.findViewById(R.id.seen_time_text);

            }
        }
    }


    private class DeliveredAdapter extends RecyclerView.Adapter<DeliveredAdapter.MyViewHolder> {
        private JSONArray array;
        private Context mContext;
        private String themevalue;

        public DeliveredAdapter(Activity activity, JSONArray jsonObject) {
            this.array = jsonObject;
            this.mContext = activity;
        }

        @Override
        public DeliveredAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.message_info_item_delivered, parent, false);
            return new DeliveredAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final DeliveredAdapter.MyViewHolder holder, int position) {

            if (array.optJSONObject(position).optString("status").equalsIgnoreCase("Delivered")) {

                DBHandler dbHandler = new DBHandler(mContext);
                JSONObject jsonObject = dbHandler.GetUserInfo(array.optJSONObject(position).optString("toNum"));


                holder.deliveredName.setText(jsonObject.optString("Name"));
                double time = Double.valueOf(array.optJSONObject(position).optString("deliveredTime"));
                holder.delievered_time_text.setText(Utils.getDate((long) time, "dd-MM-yyyy hh:mm a"));

//
            }
        }


        @Override
        public int getItemCount() {
            return array.length();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            CircleImageView profiledelivered;
            TextView deliveredName, delievered_time_text, text_name_per;

            public MyViewHolder(View itemView) {
                super(itemView);
                profiledelivered = (CircleImageView) itemView.findViewById(R.id.profiledelivered);
                deliveredName = (TextView) itemView.findViewById(R.id.deliveredName);
                delievered_time_text = (TextView) itemView.findViewById(R.id.delievered_time_text);

            }
        }
    }


}
