package com.app.crosspost.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.app.crosspost.R;
import com.app.crosspost.agoraGroupCall.openvcall.model.ConstantApp;
import com.app.crosspost.agoraGroupCall.openvcall.ui.ChatActivity;
import com.app.crosspost.models.CallsEventModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class AcceptActivity extends AppCompatActivity {
    String channel_id;
    ImageView rejectCall;
    ImageView acceptCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept);
        acceptCall=(ImageView)findViewById(R.id.acceptCall);
        rejectCall=(ImageView)findViewById(R.id.rejectCall);
        channel_id=getIntent().getStringExtra("channel_id");
        acceptCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAcceptCallClicked();
            }
        });


        rejectCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onReject();
            }
        });


    }

    public void onAcceptCallClicked()
    {
        Intent i = new Intent(AcceptActivity.this, ChatActivity.class);
        i.putExtra("call_type",getIntent().getStringExtra("call_type"));
        i.putExtra("isOutgoing", "false");
        i.putExtra(ConstantApp.ACTION_KEY_CHANNEL_NAME, channel_id);
        i.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_KEY, "");
        i.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_MODE, "AES-128-XTS");
        this.startActivity(i);
        finish();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CallsEventModel event) {

        if (event.getType().equalsIgnoreCase("endCall")) {
            finishCall();
        } else if (event.getType().equalsIgnoreCase("acceptCall")) {

        }

    }


    public void finishCall()
    {
        finish();
    }


    public void onReject()
    {
        finish();
    }

}
