package com.app.crosspost.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.crosspost.DBHelper.DBHandler;
import com.app.crosspost.R;
import com.app.crosspost.agoraGroupCall.openvcall.model.ConstantApp;
import com.app.crosspost.agoraGroupCall.openvcall.ui.ChatActivity;
import com.app.crosspost.baseUtils.AsyncTaskCompleteListener;
import com.app.crosspost.baseUtils.Const;
import com.app.crosspost.baseUtils.PostHelper;
import com.app.crosspost.baseUtils.SharedHelper;
import com.app.crosspost.fragment.ContactFragment;
import com.app.crosspost.models.ContactsModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class GroupCallActivity extends AppCompatActivity {

    GroupCallActivity.ContactAdapterList contactAdapterList;
    List<JSONObject> participantsList;
    List<JSONObject> participants_List;
    String type;
    DBHandler dbHandler;
    private RecyclerView contact_list;
    private ImageView next;
    private String TAG = GroupCallActivity.class.getSimpleName();

    public static int getPrimaryCOlor(Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }

    public static void customView(View v, int backgroundColor, int borderColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[]{100, 100, 100, 100, 100, 100, 100, 100});
        shape.setColor(backgroundColor);
        shape.setStroke(3, borderColor);
        v.setBackgroundDrawable(shape);
    }

    private void Setheme(String themevalue) {
        switch (themevalue) {
            case "1":
                setTheme(R.style.AppThemeGreen);
                break;
            case "2":
                setTheme(R.style.AppThemeBlue);
                break;
            case "3":
                setTheme(R.style.AppThemeIndigo);
                break;
            case "4":
                setTheme(R.style.AppThemeGrey);
                break;
            case "5":
                setTheme(R.style.AppThemeYellow);
                break;
            case "6":
                setTheme(R.style.AppThemeOrange);
                break;
            case "7":
                setTheme(R.style.AppThemePurple);
                break;
            case "8":
                setTheme(R.style.AppThemePaleGreen);
                break;
            case "9":
                setTheme(R.style.AppThemelightBlue);
                break;
            case "10":
                setTheme(R.style.AppThemePink);
                break;
            case "11":
                setTheme(R.style.AppThemelightGreen);
                break;
            case "12":
                setTheme(R.style.AppThemelightRed);
                break;
            default:
                setTheme(R.style.AppThemeGreen);
                break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String themevalue = SharedHelper.getKey(this, "theme_value");
        Setheme(themevalue);
        setContentView(R.layout.activity_group_call);
        type=getIntent().getStringExtra("type");
        participantsList = new ArrayList<>();
        participants_List = new ArrayList<>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.group_toolbar);
        toolbar.setBackgroundColor(getPrimaryCOlor(GroupCallActivity.this));
        dbHandler = new DBHandler(GroupCallActivity.this);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolbar.setTitle("New Group Call");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitle("Add participants");
        contact_list = (RecyclerView) findViewById(R.id.contact_list);
        next = (ImageView) findViewById(R.id.next);

//        Log.d("onCreate: ", "came2:" + ContactFragment.list_of_contacts.size());
//        Log.e("group", "" + ContactFragment.list_of_contacts.toString());

        if (ContactFragment.list_of_contacts.size() > 0) {
            contactAdapterList = new ContactAdapterList(GroupCallActivity.this, ContactFragment.list_of_contacts);
            contact_list.setHasFixedSize(true);
            contact_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            contact_list.setAdapter(contactAdapterList);
        } else {

        }
        customView(next, getPrimaryCOlor(GroupCallActivity.this), getPrimaryCOlor(GroupCallActivity.this));

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONArray jsonArray = new JSONArray(participantsList);
                JSONArray finalArray = new JSONArray();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("to", jsonArray.optJSONObject(i).optString("zoeChatId"));
                    } catch (JSONException e) {

                    }
                    finalArray.put(jsonObject);

                }

                if (jsonArray.length() <= 0) {

                    Toast.makeText(GroupCallActivity.this, getResources().getString(R.string.empty_part), Toast.LENGTH_SHORT).show();
                } else {

                    try {
                        placeGroupCall(finalArray.toString());
                    } catch (JSONException e) {

                    }

                    Log.d(TAG, "onClick: " + finalArray.toString());

                }
            }
        });
    }

    private void placeGroupCall(final String finalArray) throws JSONException {
        final String channel_id = UUID.randomUUID().toString();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("to", finalArray);
        jsonObject.put("channelId", channel_id);
        if (type.equalsIgnoreCase("voicecall")) {
            jsonObject.put("isVideoCall", "false");
        } else {
            jsonObject.put("isVideoCall", "true");

        }
        jsonObject.put("from", SharedHelper.getKey(GroupCallActivity.this, "id"));
        Log.d(TAG, "placeGroupCall: "+jsonObject);

        new PostHelper(Const.Methods.GROUP_CALL, jsonObject.toString(), Const.ServiceCode.GROUP_CALL, GroupCallActivity.this, new AsyncTaskCompleteListener() {
            @Override
            public void onTaskCompleted(JSONObject response, int serviceCode) {
                Intent i = new Intent(GroupCallActivity.this, ChatActivity.class);
                i.putExtra("call_type",type);
                i.putExtra("isOutgoing", "true");
                i.putExtra("participants", finalArray);
                i.putExtra(ConstantApp.ACTION_KEY_CHANNEL_NAME, channel_id);
                i.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_KEY, "");
                i.putExtra(ConstantApp.ACTION_KEY_ENCRYPTION_MODE, "AES-128-XTS");
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();


            }
        });

    }


    public class ContactAdapterList extends RecyclerView.Adapter<GroupCallActivity.ContactAdapterList.MyViewHolder> {
        private List<ContactsModel> list;
        private Context mContext;

        public ContactAdapterList(GroupCallActivity newGroup_activity, List<ContactsModel> list_of_contacts) {
            this.list = list_of_contacts;
            this.mContext = newGroup_activity;
        }

        @Override
        public GroupCallActivity.ContactAdapterList.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(mContext).inflate(R.layout.contact_list_item, parent, false);
            return new GroupCallActivity.ContactAdapterList.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final GroupCallActivity.ContactAdapterList.MyViewHolder holder, final int position) {

            if (list.get(position).getImage().equalsIgnoreCase("") || list.get(position).getImage().equalsIgnoreCase(" ")) {

                Picasso.with(mContext).load(R.drawable.ic_account_circle)
                        .error(mContext.getResources().getDrawable(R.drawable.ic_account_circle)).into(holder.user_image);
            } else {
                Picasso.with(mContext).load(list.get(position).getImage()).into(holder.user_image);
            }


            holder.user_name.setText(list.get(position).getName());
            holder.user_status.setText(list.get(position).getStatus());
            if (list.get(position).getSelected()) {
                holder.click.setVisibility(View.VISIBLE);
            } else {
                holder.click.setVisibility(View.GONE);

            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    if (holder.click.getVisibility() == View.VISIBLE) {
                        String clickId = list.get(position).getZeoChatId();
                        if (participantsList.size() == 1) {
                            participantsList.clear();
                            participants_List.clear();
                        } else {
                            for (int i = 0; i < participantsList.size(); i++) {
                                String id = participantsList.get(i).optString("zoeChatId");
                                if (id.equalsIgnoreCase(clickId)) {
                                    participantsList.remove(i);
                                    participants_List.remove(i);
                                    break;
                                }

                            }

                        }

//                        holder.click.setVisibility(View.GONE);
                        list.get(position).setSelected(false);
                        notifyDataSetChanged();
                        Log.d("addedList", "" + participantsList);
                        Log.d("select_list", "" + participants_List);
                        //addedAdapter.notifyDataSetChanged();

                    } else if (holder.click.getVisibility() == View.GONE) {

                        JSONObject jsonObject = new JSONObject();
                        JSONObject pat_Obj = new JSONObject();
                        try {
                            jsonObject.put("name", list.get(position).getName());
                            jsonObject.put("image", list.get(position).getImage());
                            jsonObject.put("zoeChatId", list.get(position).getZeoChatId());

                            pat_Obj.put("participantId", list.get(position).getZeoChatId());
                            if (list.get(position).getZeoChatId().equalsIgnoreCase(SharedHelper.getKey(GroupCallActivity.this, "my_zoe_id"))) {
                                pat_Obj.put("isAdmin", "1");
                            } else {
                                pat_Obj.put("isAdmin", "0");
                            }
                            jsonObject.put("status", list.get(position).getStatus());
                            participantsList.add(jsonObject);
                            participants_List.add(pat_Obj);
                            list.get(position).setSelected(true);
                            notifyDataSetChanged();
                            Log.d("addedList", "" + participantsList);
                            Log.d("select_list", "" + participants_List);
                            //addedAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                }
            });
        }

        @Override
        public int getItemCount() {
            return list.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            CircleImageView user_image;
            TextView user_name, user_status;
            ImageView click;

            public MyViewHolder(View itemView) {
                super(itemView);
                user_image = (CircleImageView) itemView.findViewById(R.id.contact_image);
                user_name = (TextView) itemView.findViewById(R.id.user_name);
                user_status = (TextView) itemView.findViewById(R.id.user_status);
                click = (ImageView) itemView.findViewById(R.id.remove_view_check);
                customView(click, getPrimaryCOlor(GroupCallActivity.this), getPrimaryCOlor(GroupCallActivity.this));


            }
        }
    }
}
