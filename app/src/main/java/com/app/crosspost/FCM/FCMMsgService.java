package com.app.crosspost.FCM;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.app.crosspost.DBHelper.DBHandler;
import com.app.crosspost.R;
import com.app.crosspost.Service.NotificationReceiver;
import com.app.crosspost.activity.AcceptActivity;
import com.app.crosspost.activity.MainActivity;
import com.app.crosspost.activity.VideoChatViewActivity;
import com.app.crosspost.baseUtils.Const;
import com.app.crosspost.baseUtils.SharedHelper;
import com.app.crosspost.models.CallsEventModel;
import com.app.crosspost.models.CallsModel;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by KrishnaDev on 1/13/17.
 */

public class FCMMsgService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    public static String current_id = "", check_id;
    DBHandler dbHandler = new DBHandler(this);
    String Notifytime;
    private Handler handler = new Handler();
    private int value = 0;
    JSONObject njsonObj;
    public static final String ACTION_1 = "action_1";
    public static final String NOTIFICATION_REPLY = "notification_reply";
    Uri defaultSoundUri;
    long[] vib_val;
    int led_light;
    String name;
    NotificationCompat.Builder notificationBuilder;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Create and show notification
        Log.e("Notification_msg", remoteMessage.getFrom());
        //Log.e("Notification_msg_title", remoteMessage.getNotification().getTitle());
        JSONObject jsonObject = new JSONObject(remoteMessage.getData());
        try {
            sendNotification(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

    @SuppressWarnings("deprecation")
    private void sendNotification(JSONObject jsonObject) throws JSONException {

        njsonObj = jsonObject;
        Log.e("sendNotify_method", jsonObject.toString());
        String zoeChatId = jsonObject.optString("fromId");
        String channel_id = jsonObject.optString("channel_id");


        name = dbHandler.GetUserName(zoeChatId);
        String image = dbHandler.GetUserImage(zoeChatId);

        long current = System.currentTimeMillis();
        try {
            JSONObject values1 = new JSONObject(jsonObject.optString("payload"));
            String channelId = values1.optString("channelId");
            Notifytime = dbHandler.GettimedNotifications(channelId);
            long noti = Long.parseLong(Notifytime);
            if (Notifytime.equalsIgnoreCase("1")) {
                Notifytime = "1";
            } else if (Notifytime.equalsIgnoreCase("0")) {
                Notifytime = "0";
            } else {
                if (current >= noti) {
                    Notifytime = "0";
                    dbHandler.UpdateNotifications(channelId, "0");
                } else {
                    Notifytime = "0";
                }
            }
        } catch (Exception e) {

        }

        if (jsonObject.optString("pushType").equalsIgnoreCase("voicecall")) {
            String call_msg = name + " " + "Voice Calling...";
            String callType = "Voice Calling...";
            String type = "voiceCall";
            dbHandler.InsertCalls(new CallsModel(zoeChatId, name, "", System.currentTimeMillis(), type, "1"));
            Intent call = new Intent(this, VideoChatViewActivity.class);
            call.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            call.putExtra("call", type);
            call.putExtra("zoeChatID", zoeChatId);
            call.putExtra("user_name", name);
            call.putExtra("image", image);
            call.putExtra("channelId", channel_id);
            call.putExtra("receive", "yes");
            this.startActivity(call);


            addNotification(callType, type);
           /* Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setVibrate(Const.URI.default_call_vibrate)
                    .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                    .setContentTitle(name)
                    .setContentText(call_msg)

                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);


            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 *//* ID of notification *//*, notificationBuilder.build());*/
        } else if (jsonObject.optString("pushType").equalsIgnoreCase("videocall")) {
            String call_msg = name + " " + "Video Calling...";
            String callType = "Video Calling...";
            String type = "videoCall";
            dbHandler.InsertCalls(new CallsModel(zoeChatId, name, "", System.currentTimeMillis(), type, "1"));
            Intent call = new Intent(this, VideoChatViewActivity.class);
            call.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            call.putExtra("call", type);
            call.putExtra("zoeChatID", zoeChatId);
            call.putExtra("user_name", name);
            call.putExtra("image", image);
            call.putExtra("channelId", channel_id);
            call.putExtra("receive", "yes");
            this.startActivity(call);


            addNotification(callType, type);
            /*Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                    .setContentTitle(name)
                    .setContentText(call_msg)
                    .setVibrate(Const.URI.default_call_vibrate)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);


            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 *//* ID of notification *//*, notificationBuilder.build());*/
        } else if (jsonObject.optString("pushType").equalsIgnoreCase("groupvideocall")) {
//            String call_msg = name + " " + "Group Call ...";
//            String type = "groupvideocall";
//            dbHandler.InsertCalls(new CallsModel(zoeChatId, name, "", System.currentTimeMillis(), type, "1"));
//            Intent call = new Intent(this, VideoChatViewActivity.class);
//            call.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            call.putExtra("call", type);
//            call.putExtra("zoeChatID", zoeChatId);
//            call.putExtra("user_name", name);
//            call.putExtra("image", image);
//            call.putExtra("channelId", channel_id);
//            call.putExtra("receive", "yes");
//            this.startActivity(call);

            String call_msg = name + " " + "Group Call ...";
            Intent i = new Intent(FCMMsgService.this, AcceptActivity.class);
            i.putExtra("channel_id", channel_id);
            i.putExtra("call_type", "video");
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(i);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                    .setContentTitle(name)
                    .setContentText(call_msg)
                    .setVibrate(Const.URI.default_call_vibrate)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);


            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        } else if (jsonObject.optString("pushType").equalsIgnoreCase("groupvoicecall")) {
//            String call_msg = name + " " + "Group Call ...";
//            String type = "groupvideocall";
//            dbHandler.InsertCalls(new CallsModel(zoeChatId, name, "", System.currentTimeMillis(), type, "1"));
//            Intent call = new Intent(this, VideoChatViewActivity.class);
//            call.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            call.putExtra("call", type);
//            call.putExtra("zoeChatID", zoeChatId);
//            call.putExtra("user_name", name);
//            call.putExtra("image", image);
//            call.putExtra("channelId", channel_id);
//            call.putExtra("receive", "yes");
//            this.startActivity(call);

            String call_msg = name + " " + "Group Call ...";
            Intent i = new Intent(FCMMsgService.this, AcceptActivity.class);
            i.putExtra("channel_id", channel_id);
            i.putExtra("call_type", "voicecall");
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            this.startActivity(i);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                    .setContentTitle(name)
                    .setContentText(call_msg)
                    .setVibrate(Const.URI.default_call_vibrate)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri);


            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
        }


        if (!jsonObject.optString("message").isEmpty()) {


            int single_value = Integer.parseInt(SharedHelper.getKey(getApplicationContext(), "single_vib_value"));
            int grp_value = Integer.parseInt(SharedHelper.getKey(getApplicationContext(), "group_vib_value"));
            int call_value = Integer.parseInt(SharedHelper.getKey(getApplicationContext(), "call_vib_value"));
            String light = SharedHelper.getKey(getApplicationContext(), "single_light_value");
            String g_light = SharedHelper.getKey(getApplicationContext(), "group_light_value");
            String in_sound = SharedHelper.getKey(getApplicationContext(), "play_sounds");


            Const.URI.default_single_vibrate = new long[]{single_value, single_value, single_value, single_value};
            Const.URI.default_group_vibrate = new long[]{grp_value, grp_value, grp_value, grp_value};
            Const.URI.default_call_vibrate = new long[]{call_value, call_value, call_value, call_value};
            Const.URI.single_light = Integer.parseInt(light);
            Const.URI.group_light = Integer.parseInt(g_light);
            Const.URI.inMessageTone = in_sound.equalsIgnoreCase("yes");
            Const.URI.default_single_message = Uri.parse(SharedHelper.getKey(this, "single_noti_tone"));
            Const.URI.default_group_message = Uri.parse(SharedHelper.getKey(this, "group_noti_tone"));


            Log.d(TAG, "sendNotification: values:" + jsonObject.optString("chatRoomType"));

            JSONObject values = new JSONObject(jsonObject.optString("payload"));
            Log.d(TAG, "sendNotification: chat:" + values);
            if (values.optString("chatRoomType").equalsIgnoreCase("0")) {
                check_id = jsonObject.optString("fromId");
                defaultSoundUri = Const.URI.default_single_message;
                vib_val = Const.URI.default_single_vibrate;
                led_light = Const.URI.single_light;
            } else {
                check_id = values.optString("groupId");
                defaultSoundUri = Const.URI.default_group_message;
                vib_val = Const.URI.default_group_vibrate;
                led_light = Const.URI.group_light;

            }
            Log.e(TAG, "sendNotificationout: " + check_id + "," + current_id);


            if (Notifytime.equalsIgnoreCase("0") || Notifytime.equalsIgnoreCase("")) {
                long currenttime = System.currentTimeMillis();
                value = value + 1;
                if (!check_id.equalsIgnoreCase(current_id) || current_id.equalsIgnoreCase("")) {

                    if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.M) {
                        // Do something for Nougat and above versions
                        InLineReplyExample();
                    } else {

                        Intent intent = new Intent(this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                                PendingIntent.FLAG_ONE_SHOT);
                        NotificationCompat.InboxStyle inboxStyle =
                                new NotificationCompat.InboxStyle();
                        // Sets a title for the Inbox in expanded layout
                        inboxStyle.setBigContentTitle("Title - Notification");
                        inboxStyle.setSummaryText("You have " + value + " Notifications.");


                        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                                .setContentTitle(name)
                                .setLights(led_light, 500, 500)
                                .setContentText(jsonObject.optString("message"))
                                .setAutoCancel(true)
                                .setVibrate(vib_val)
                                .setSound(defaultSoundUri)
                                .setContentIntent(pendingIntent);

                        NotificationManager notificationManager =
                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
                    }
                }
            }


        }
        if (jsonObject.optString("pushType").equalsIgnoreCase("acceptCall")) {

            EventBus.getDefault().post(new CallsEventModel("acceptCall", channel_id));
            addNotification();
        }
        if (jsonObject.optString("pushType").equalsIgnoreCase("out")) {

            notificationClear();
            EventBus.getDefault().post(new CallsEventModel("endCall"));


        } else if (jsonObject.optString("pushType").equalsIgnoreCase("inc")) {

            notificationClear();
            EventBus.getDefault().post(new CallsEventModel("endCall"));


        }


        if (jsonObject.optString("pushType").equalsIgnoreCase("GroupAdd")) {

            String createdBy = dbHandler.GetUserName(jsonObject.optString("createdBy"));
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri = Const.URI.default_group_message;
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setColor(this.getResources().getColor(R.color.colorPrimaryDark))
                    .setContentTitle(jsonObject.optString("groupName"))
                    .setContentText(createdBy + " added you")
                    .setAutoCancel(true)
                    .setVibrate(Const.URI.default_group_vibrate)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());


        }
    }

    private void notificationClear() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
        }
    }

    private void addNotification() {
        notificationClear();

        Intent intent = new Intent(this, VideoChatViewActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(name)
                .setContentText("Ongoing Call")
                .setColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
                .setContentIntent(pendingIntent)
                //.setSound(defaultSoundUri)
                .setSound(null)
                .setVibrate(null)
                //.setVibrate(Const.URI.default_call_vibrate)
                .setUsesChronometer(true)
                .setAutoCancel(true);

        Intent videoCall = new Intent(this, NotificationReceiver.class);
        // videoCall.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        videoCall.putExtra("userId", "2");
        videoCall.putExtra("chatroomtype", "rejectcall");
        videoCall.putExtra("call", " ");

        PendingIntent videoPendingIntent = PendingIntent.getBroadcast(this,
                1, videoCall, PendingIntent.FLAG_UPDATE_CURRENT);

        notificationBuilder.addAction(R.drawable.ic_call_reject, "Hang Up", videoPendingIntent);


        Notification n = notificationBuilder.build();
        n.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;

        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(0 /* ID of notification */, n);
        }
    }

    public void addNotification(String call_msg, String type) {

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent voiceCallIntent = new Intent(this, NotificationReceiver.class);

        voiceCallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        voiceCallIntent.putExtra("userId", "1");
        voiceCallIntent.putExtra("chatroomtype", "acceptcall");
        voiceCallIntent.putExtra("call", type);

        PendingIntent voicePendingIntent = PendingIntent.getBroadcast(this,
                1, voiceCallIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        Intent videoCall = new Intent(this, NotificationReceiver.class);
        videoCall.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        videoCall.putExtra("userId", "2");
        videoCall.putExtra("chatroomtype", "rejectcall");
        videoCall.putExtra("call", type);

        PendingIntent videoPendingIntent = PendingIntent.getBroadcast(this,
                2, videoCall, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(name)
                .setContentText(call_msg)
                .setColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
                .setSound(defaultSoundUri)
                .setVibrate(Const.URI.default_call_vibrate)
                .setAutoCancel(true)
                .setUsesChronometer(false)
                .setPriority(Notification.PRIORITY_MAX)
                .setPriority(Notification.PRIORITY_HIGH)
                .addAction(R.drawable.ic_call_accept, "Accept", voicePendingIntent)
                .addAction(R.drawable.ic_call_reject, "Reject", videoPendingIntent);


        Notification n = notificationBuilder.build();
        n.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;

        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(0 /* ID of notification */, n);
        }
    }


    public void InLineReplyExample() {
        //Provide receiver class to handle the response
        String chatType = "";

        Log.e(TAG, "InLineReplyExample: " + njsonObj);
        try {
            JSONObject jsonObject = new JSONObject(njsonObj.toString());
            JSONObject values1 = new JSONObject(jsonObject.optString("payload"));
            chatType = values1.optString("chatRoomType");

        } catch (JSONException e) {

        }


        Intent intent = new Intent(this, NotificationReceiver.class);
        intent.putExtra("userId", njsonObj.optString("fromId"));
        intent.putExtra("chatroomtype", chatType);

        intent.setAction(ACTION_1);

        Bundle bundle = new Bundle();
        bundle.putCharSequence("userId", njsonObj.optString("fromId"));
        bundle.putCharSequence("chatroomtype", chatType);


        PendingIntent detailsPendingIntent = PendingIntent.getBroadcast(
                this,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        Bundle bundle1 = new Bundle();
        bundle1.putString("yes", "sasdsa");
        RemoteInput remoteInput = new RemoteInput.Builder(NOTIFICATION_REPLY)
                .setLabel("Texel")
                .addExtras(bundle1)
                .build();


        //Setup action item
        NotificationCompat.Action action =
                new NotificationCompat.Action.Builder(android.R.drawable.ic_delete,
                        "Reply Now...", detailsPendingIntent)
                        .addRemoteInput(remoteInput)
                        .addExtras(bundle)

                        .build();

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(name)
                .setContentText(njsonObj.optString("message"))
                .setAutoCancel(true)
                .setVibrate(vib_val)
                .setSound(defaultSoundUri)
                .setColor(getResources().getColor(R.color.white))
                .setContentIntent(detailsPendingIntent)
                .addAction(action);
//                .addAction(android.R.drawable.ic_menu_compass, "More", detailsPendingIntent)
//                .addAction(android.R.drawable.ic_menu_directions, "Help", detailsPendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(112, mBuilder.build());

    }


    public static class NotificationActionService extends IntentService {
        public NotificationActionService() {
            super(NotificationActionService.class.getSimpleName());
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            Log.d(TAG, "onHandleIntent: " + intent.getAction());
            String action = intent.getAction();
            if (ACTION_1.equals(action)) {
                Intent intent1 = new Intent(NotificationActionService.this, MainActivity.class);
                startActivity(intent1);
                // TODO: handle action 1.
                // If you want to cancel the notification: NotificationManagerCompat.from(this).cancel(NOTIFICATION_ID);
            }
        }
    }


}
