package com.app.crosspost.baseUtils;

import org.json.JSONObject;

/**
 * Created by KrishnaDev on 1/10/17.
 */
public interface OnBottomReached {
    void onBottomReached();
}
