package com.app.crosspost.baseUtils;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.app.crosspost.R;
import com.app.crosspost.agoraGroupCall.openvcall.model.CurrentUserSettings;
import com.app.crosspost.agoraGroupCall.openvcall.model.WorkerThread;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;
import io.socket.client.IO;
import io.socket.client.Socket;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import vc908.stickerfactory.StickersManager;
import vc908.stickerfactory.User;

//import cafe.adriel.androidaudioconverter.AndroidAudioConverter;
//import cafe.adriel.androidaudioconverter.callback.ILoadCallback;

/**
 * Created by KrishnaDev on 1/10/17.
 */

public class AppController extends Application {

    public static Application application;
    public static Resources resources;
    private static Context mContext;
    private static Socket ChatSocket;
    public static boolean is_in_action_mode = false;
    public static int numOfItemsSelected = 0;
    public static int whichRecylerSelected = 0;
    private static AppController mInstance;




    //This is to check whether the pinned adapter is in action mode
    public static boolean isPinnedInActionMode=false;
    public static boolean isNormalChatInActionMode=false;


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }

    private WorkerThread mWorkerThread;

    public synchronized void initWorkerThread() {
        if (mWorkerThread == null) {
            mWorkerThread = new WorkerThread(getApplicationContext());
            mWorkerThread.start();

            mWorkerThread.waitForReady();
        }
    }

    public synchronized WorkerThread getWorkerThread() {
        return mWorkerThread;
    }

    public synchronized void deInitWorkerThread() {
        mWorkerThread.exit();
        try {
            mWorkerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mWorkerThread = null;
    }

    public static final CurrentUserSettings mVideoSettings = new CurrentUserSettings();

    public static synchronized AppController getInstance() {
        return mInstance;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
        mContext = this;
        mInstance=this;


    }
    public static Context getContext(){
        return mContext;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        Fabric.with(this, new Crashlytics());
        StickersManager.initialize("c67495c61079060965c5c6b47064e452", this);
        Map<String, String> meta = new HashMap<>();
        meta.put(User.KEY_GENDER, User.GENDER_FEMALE);
        meta.put(User.KEY_AGE, "33");
        // Put your user id when you know it
        StickersManager.setUser(vc908.stickerfactory.utils.Utils.getDeviceId(this), meta);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
        Log.d("Appcontroller", "start");


        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("Helvetica-Normal.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

    }

//    public static Socket getSocket() {
//        return ChatSocket;
//    }



}
